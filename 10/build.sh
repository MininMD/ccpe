rm *.log
rm *.aux
rm *.out
rm *.synctex.gz

mkdir pdf
for i in *.tex; do
	xelatex --output-directory pdf "$i"
	xelatex --output-directory pdf "$i"
done

cd pdf
rm *.log
rm *.aux
rm *.out
rm *.synctex.gz
